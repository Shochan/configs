if status is-interactive
    # Commands to run in interactive sessions can go here
 set fish_greeting
end
## MY ALIAS LIST	
alias yy="sudo pacman -Syyu && yay"
alias off="sudo shutdown now"

### SET EITHER DEFAULT EMACS MODE OR VI MODE ###
function fish_user_key_bindings
  # fish_default_key_bindings
  fish_vi_key_bindings
end
### END OF VI MODE ###

### AUTOCOMPLETE AND HIGHLIGHT COLORS ###
set fish_color_normal '#FFFBCA'
set fish_color_autosuggestion '#AEEA94'
set fish_color_command '#FFFBCA'
set fish_color_error '#ff6c6b'
set fish_color_param brcyan