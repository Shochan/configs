# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from libqtile import bar, layout, qtile, widget, hook, extension
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from libqtile.widget.textbox import TextBox
import os
import subprocess
from typing import List

mod = "mod4"
terminal = guess_terminal()

keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows
    Key([mod], "Left", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "Right", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "Down", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "Up", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "Left", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "Right", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "Down", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "Left", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "Right", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "Down", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "Up", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key(
        [mod, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod, "shift"], "q", lazy.window.kill(), desc="Kill focused window"),
    Key(
        [mod],
        "f",
        lazy.window.toggle_fullscreen(),
        desc="Toggle fullscreen on the focused window",
    ),
    Key([mod], "t", lazy.window.toggle_floating(), desc="Toggle floating on the focused window"),
    Key([mod, "shift"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "r", lazy.reload_restart(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),

    #lock screen
    Key([mod], "l", lazy.spawn("i3lock -i /home/karthzura/Pictures/wallpaper-collection/karasuno.png"), desc="Lock the session"),
    
    #Firefox
    Key([mod, "shift"], "f",
             lazy.spawn("firefox"),
             desc='Open Firefox'
             ),
    #Tor
    Key([mod, "shift"], "t",
             lazy.spawn("torbrowser-launcher"),
             desc='Open Tor Browser'
             ),
    #
    #Open Virtual box
    Key([mod, "shift"], "v",
             lazy.spawn("virtualbox"),
             desc='Open Virtualbox'
             ),

    #Flameshot
    Key([], "F1",
             lazy.spawn("flameshot gui"),
             desc='Open Flameshot'
             ),

    #decrease brightness
    Key([], "F2",
             lazy.spawn("lux -s 10%"),
             desc='decrease brightness'
             ),

    #increase brightness
    Key([], "F3",
             lazy.spawn("lux -a 10%"),
             desc='increase brightness'
             ),

    #decrease volume
    Key([], "F7",
             lazy.spawn("pactl set-sink-volume 0 -10%"),
             desc='decrease volume'
             ),
    #increase volume
    Key([], "F8",
             lazy.spawn("pactl set-sink-volume 0 +10%"),
             desc='increase volume'
             ),
        #Open Dmenu
    Key([mod], "d",
            lazy.spawn("dmenu_run -i -nb '#191919' -nf '#B060DD' -sb '#B060DD' -sf '#191919' -fn 'Mononoki Nerd Font:bold:pixelsize=20'"),
            desc='Open Dmenu'
                ),
    #open File Manager
    Key([mod], "t",
            lazy.spawn("thunar"),
             desc='Open thunar'
             ),
]

# Add key bindings to switch VTs in Wayland.
# We can't check qtile.core.name in default config as it is loaded before qtile is started
# We therefore defer the check until the key binding is run by using .when(func=...)
for vt in range(1, 8):
    keys.append(
        Key(
            ["control", "mod1"],
            f"f{vt}",
            lazy.core.change_vt(vt).when(func=lambda: qtile.core.name == "wayland"),
            desc=f"Switch to VT{vt}",
        )
    )


#groups = [Group(i) for i in "123456789"]

groups = []
group_names = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
group_labels = [" ", " ", " ", " ", " ", " ", " ", " ", " "]
group_layouts = ["max", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall"]

for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
               ))


for i in groups:
    keys.extend(
        [
            # mod + group number = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod + shift + group number = switch to & move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(i.name),
            ),
            # Or, use below if you prefer not to switch to that group.
            # # mod + shift + group number = move focused window to group
            # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            #     desc="move focused window to group {}".format(i.name)),
        ]
    )

layouts = [
    layout.Columns(border_focus_stack=["#d75f5f", "#8f3d3d"], border_width=4),
    layout.Max(name = 'Max'),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    layout.MonadTall(border_focus = '#8624BF', margin = 7, name = 'MonadTall'),
    layout.MonadWide(border_focus = '#8624BF', margin = 7, name = 'MonadWide'),
    layout.Bsp(name = 'BSP', margin = 7),
    # layout.Matrix(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font="mononoki NF Bold",
    fontsize=13,
    padding=0,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.GroupBox(
                    font = "mononoki NF Bold",
                    fontsize = 15,
                    margin_y = 0,
                    margin_x = 0,
                    Padding_y = 9,
                    padding_x = 5,
                    borderwidth = 3,
                    disable_drag = True,
                    active = "#CCE7E8",
                    rounded = False,
                    highlight_color = '#da50b2',
                    highlight_method = "text",
                    this_current_screen_border = '#da50b2',
                    foreground = "#da50b2",
                    background = "#000000"
                    ),

                widget.WindowName(
                    font = 'mononoki NF Bold',
                    foreground = '#cce7e8'
                    ),

                widget.TextBox(
                       text = '\uE0B2',
                       background = "#000000",
                       foreground = "#ff0074",
                       padding = 0,
                       fontsize = 26
                       ),

                #widget.TextBox(
                    #foreground = "#cce7e8",
                    #background = "#cce7e8",
                    #fontsize = 67
                    #size_precent = 100,
                    #padding = 40
                    #),

                 widget.Battery(
                       charge_char = ' ',
                       discharge_char = ' ',
                       empty_char = ' ',
                       full_char = ' ',
                       show_short_text = False,
                       font = 'mononoki NF Bold',
                       low_percentage = 0.15,
                       low_foreground = '#ea1717',
                       format = "{char} {percent:2.0%} ",
                       #update_interval = '0',
                       foreground = '#000000',
                       background = '#ff0074'
                       ),

                widget.TextBox(
                    text = '\uE0B2',
                    background = "#ff0074",
                    foreground = "#000957",
                    padding = 0,
                    fontsize = 26
                       ),
                
                  widget.CPU(
                 format = ' CPU {load_percent}% ',
                 foreground = "#ffffff",
                 background = "#000957",
                       ),               
  
               widget.TextBox(
                    text = '\uE0B2',
                    background = "#000957",
                    foreground = "#00f9ff",
                    padding = 0,
                    fontsize = 26
                       ),


                #widget.TextBox(
                    #text = ' ',
                    #background = "#00f9ff",
                    #foreground = "#000000",
                    #padding = 0,
                    #fontsize = 14
                       #),

               #widget.PulseVolume(
                    #foreground = "#000000",
                    #background = "#00f9ff",
                    #emoji = ' ',
                    #fmt = 'Vol: {}',
                    #font = 'mononoki NF Bold',
                    #volume_app = "pavucontrol",
                    #padding = 5
                    #),
                
                widget.Memory(
                 foreground = "#000000",
                 background = "#00f9ff",
                 padding = 0,
                 format = '{MemUsed: .0f}{mm}',
                 fmt = '🖥 Mem{} '
                     ),

                widget.TextBox(
                    text = '\uE0B2',
                    background = "#00ff9f",
                    foreground = "#ff4d00",
                    padding = 0,
                    fontsize = 26
                    ),

                widget.TextBox(
                    text = '☯',
                    background = "#ff4d00",
                    foreground = "#cce7e8",
                    padding = 0,
                    fontsize = 14
                    ),

                widget.CurrentLayout(
                    background = "#ff4d00",
                    foreground = "#cce7e8",
                    font = 'mononoki NF Bold'
                    ),

                widget.TextBox(
                    text =  '\uE0B2',
                    #mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn(terminal + ' -e nm-applet &')},
                    background = "#ff4d00",
                    foreground = "#00ff6b",
                    padding = 0,
                    fontsize = 26
                       ),


                widget.Net(
                    interface = "wlo1",
                    format = '✲ {down} ↓↑ {up} ',
                    font = 'mononoki NF Bold',
                    foreground = "#000000",
                    background = "#00ff6b",
                    padding = 0
                       ),

                widget.TextBox(
                        text =  '\uE0B2',
                       background = "#00ff6b",
                       foreground = "#a300ff",
                       padding = 0,
                       fontsize = 26
                       ),

                widget.TextBox(
                    text = '',
                    background = "#a300ff",
                    foreground = "#ffffff",
                    padding = 0,
                    fontsize = 17
                       ),

                widget.Clock(
                    format=' %B %d, %Y %A %I:%M %p',
                    font = 'mononoki NF Bold',
                    background = "#a300ff",
                    foreground = "#ffffff"
                     ),
                
                widget.Systray(
                    background = "#791AB0"
                     ),
            ],
            24,
            opacity=0.5
            # border_width=[2, 0, 2, 0],  # Draw top and bottom borders
            # border_color=["ff00ff", "000000", "ff00ff", "000000"]  # Borders are magenta
        ),
        # You can uncomment this variable if you see that on X11 floating resize/moving is laggy
        # By default we handle these events delayed to already improve performance, however your system might still be struggling
        # This variable is set to None (no cap) by default, but you can set it to 60 to indicate that you limit it to 60 events per second
        # x11_drag_polling_rate = 60,
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
floats_kept_above = True
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# xcursor theme (string or None) and size (integer) for Wayland backend
wl_xcursor_theme = None
wl_xcursor_size = 24

@hook.subscribe.startup_once
def autostart() :
    home = os.path.expanduser('~/.config/qtile/auto.sh')
    subprocess.call(home)

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
